import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
saiyayin!:any;
kriptoniano!:any;

  constructor() { }

  ngOnInit(): void {
  }

  recibirObjeto1($event:any):void{
    this.saiyayin = $event;
    console.log(this.saiyayin);
  }

  recibirObjeto2($event:any):void{
    this.kriptoniano = $event;
    console.log(this.kriptoniano);
    
  }
}
